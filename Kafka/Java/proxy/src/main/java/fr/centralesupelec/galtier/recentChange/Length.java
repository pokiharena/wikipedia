package fr.centralesupelec.galtier.recentChange;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Length of old and new change
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "old", "new" })
public class Length {

	/**
	 * (rc_old_len)
	 *
	 */
	@JsonProperty("old")
	@JsonPropertyDescription("(rc_old_len)")
	private Integer old;
	/**
	 * (rc_new_len)
	 *
	 */
	@JsonProperty("new")
	@JsonPropertyDescription("(rc_new_len)")
	private Integer _new;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Length() {
	}

	/**
	 *
	 * @param old
	 * @param _new
	 */
	public Length(Integer old, Integer _new) {
		super();
		this.old = old;
		this._new = _new;
	}

	/**
	 * (rc_old_len)
	 *
	 */
	@JsonProperty("old")
	public Integer getOld() {
		return old;
	}

	/**
	 * (rc_old_len)
	 *
	 */
	@JsonProperty("old")
	public void setOld(Integer old) {
		this.old = old;
	}

	/**
	 * (rc_new_len)
	 *
	 */
	@JsonProperty("new")
	public Integer getNew() {
		return _new;
	}

	/**
	 * (rc_new_len)
	 *
	 */
	@JsonProperty("new")
	public void setNew(Integer _new) {
		this._new = _new;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("old", old).append("_new", _new).toString();
	}

}
